# RECIPES

This a sanbercode Final Project.
A recipe app is a mobile program to steer you through meal preparation. 
It offers the functions to make cooking easy and enjoyable and gives step-by-step directions. 
A cooking app interactive design

## API using for this application

tasty.p.rapidapi.com/recipes/list

## APK Link
https://drive.google.com/drive/u/0/folders/1CsVGmNoIcEzQkfM89eOQgr0RdA6R0wMz

## Demo video
https://www.youtube.com/watch?v=V3b9WO2Ccs8

## Figma Mockup
https://www.figma.com/file/4XyXn2LFaxlv8URiQhFtio/Mockup-Recipes-(Final-Project-Sanbercode)?node-id=0%3A1
