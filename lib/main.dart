import 'package:flutter/material.dart';
import 'package:recipe_app/views/pages/login_screen.dart';
import 'package:firebase_core/firebase_core.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Food Recipe',
      theme: ThemeData(
          primarySwatch: Colors.blue,
          primaryColor: Colors.white,
          iconTheme: const IconThemeData(color: Colors.black),
          textTheme:
              const TextTheme(bodyText2: TextStyle(color: Colors.white))),
      home: const LoginScreen(),
    );
  }
}
