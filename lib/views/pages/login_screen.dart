import 'package:flutter/material.dart';
import 'package:recipe_app/views/pages/home.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:recipe_app/views/pages/register_page.dart';

class LoginScreen extends StatefulWidget {
  final String? email;
  const LoginScreen({Key? key, this.email}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    Color bgColor = const Color(0xFFFDF5E8);
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: bgColor,
      body: ListView(
        children: [
          Container(
            alignment: Alignment.center,
            margin: const EdgeInsets.fromLTRB(0, 80, 0, 0),
            child: Image.asset(
              "images/logo.png",
              width: 200,
              height: 200,
            ),
          ),
          Column(
            children: [
              Container(
                padding: const EdgeInsets.fromLTRB(40, 0, 0, 0),
                alignment: Alignment.bottomLeft,
                child: const Text(
                  "Username",
                  style: TextStyle(color: Colors.black),
                ),
              ),
              Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.fromLTRB(40, 5, 40, 15),
                child: TextField(
                    controller: _emailController,
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      filled: true,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(6.0),
                        borderSide: const BorderSide(color: Colors.white),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(6.0),
                        borderSide: const BorderSide(color: Colors.white),
                      ),
                      border: const OutlineInputBorder(),
                    )),
              ),
            ],
          ),
          Column(
            children: [
              Container(
                padding: const EdgeInsets.fromLTRB(40, 0, 0, 0),
                alignment: Alignment.bottomLeft,
                child: const Text("Password",
                    style: TextStyle(color: Colors.black)),
              ),
              Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.fromLTRB(40, 5, 40, 40),
                child: TextField(
                    obscureText: true,
                    controller: _passwordController,
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      filled: true,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(6.0),
                        borderSide: const BorderSide(color: Colors.white),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(6.0),
                        borderSide: const BorderSide(color: Colors.white),
                      ),
                      border: const OutlineInputBorder(),
                    )),
              ),
            ],
          ),
          Container(
            padding: const EdgeInsets.fromLTRB(40, 10, 40, 0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: const Color(0xFFB8DBD9),
                  minimumSize: const Size(20, 50),
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(6)))),
              onPressed: () async {
                try {
                  await _firebaseAuth
                      .signInWithEmailAndPassword(
                          email: _emailController.text,
                          password: _passwordController.text)
                      .then((value) => Navigator.of(context).pushReplacement(
                          MaterialPageRoute(
                              builder: (context) =>
                                  HomePage(email: _emailController.text))));
                } catch (e) {
                  var snackdemo = SnackBar(
                    content: Text(e.toString()),
                    backgroundColor: Colors.red,
                    elevation: 10,
                    behavior: SnackBarBehavior.floating,
                    margin: const EdgeInsets.all(5),
                  );
                  ScaffoldMessenger.of(context).showSnackBar(snackdemo);
                }
              },
              child: const Text(
                "Login",
                style: TextStyle(color: Colors.black),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.fromLTRB(40, 0, 0, 100),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const Text("Don't have account?",
                    style: TextStyle(color: Colors.black)),
                TextButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return const RegisterPage();
                      }));
                    },
                    child: const Text(
                      "Sign up",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ))
              ],
            ),
          ),
        ],
      ),
    );
  }
}
