import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class ProfilePage extends StatefulWidget {
  final String? email;
  const ProfilePage({this.email, Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  _ProfilePageState();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        toolbarHeight: 100,
        backgroundColor: const Color(0xFF6CBEED),
        flexibleSpace: Container(
          decoration: const BoxDecoration(
              color: Color(0xFF6CBEED),
              image: DecorationImage(
                alignment: Alignment.topLeft,
                image: AssetImage("images/Ellipse 6.png"),
              )),
        ),
        elevation: 0,
      ),
      body: Center(
        child: Column(
          children: [
            Stack(
              alignment: Alignment.topCenter,
              children: [
                Container(
                  color: const Color(0xFF6CBEED),
                  height: 100,
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(100.0),
                  child: Image.network(
                    "https://agamk24.github.io/img/agam.jpg",
                    height: 200,
                    width: 200,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              widget.email.toString(),
              style: const TextStyle(
                  color: Colors.black, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 20,
            ),
            Column(
              children: [
                SizedBox(
                  width: 350,
                  height: 50,
                  child: Card(
                    color: Colors.blue.shade300,
                    child: Row(
                      children: const [
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.facebook,
                            color: Colors.white,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text("Agamk24"),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  width: 350,
                  height: 50,
                  child: Card(
                    color: Colors.blue.shade300,
                    child: Row(
                      children: const [
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.phone,
                            color: Colors.white,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text("+62-8129-1101-7641"),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(40, 20, 0, 0),
                  alignment: Alignment.centerLeft,
                  child: const Text(
                    "About Me",
                    style: TextStyle(color: Colors.black),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(40, 20, 40, 0),
                  child: const Text(
                    "I am an enthusiastic, self-motivated, reliable, responsible and hard working person. I am a mature team worker and adaptable to all challenging situations. I am able to work well both in a team environment as well as using own initiative. I am able to work well under pressure and adhere to strict deadlines.",
                    style: TextStyle(color: Colors.black),
                    textAlign: TextAlign.justify,
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
