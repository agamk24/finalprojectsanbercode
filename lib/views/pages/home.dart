import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:recipe_app/models/recipe.dart';
import 'package:recipe_app/models/recipe_api.dart';
import 'package:recipe_app/views/pages/detail_recipe.dart';
import 'package:recipe_app/views/pages/login_screen.dart';
import 'package:recipe_app/views/pages/profile_page.dart';
import 'package:recipe_app/views/widgets/recipe_card.dart';

class HomePage extends StatefulWidget {
  final String? email;
  const HomePage({this.email, Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  _HomePageState();
  Future<void> signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  late List<Recipe> _recipe;
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    getRecipe();
  }

  Future<void> getRecipe() async {
    _recipe = await RecipeApi.getRecipe();
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
          icon: const Icon(
            Icons.logout,
            color: Colors.black,
          ),
          onPressed: () async {
            await signOut().then((value) => Navigator.of(context)
                .pushReplacement(MaterialPageRoute(
                    builder: (context) => const LoginScreen())));
          },
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
        ),
        backgroundColor: Colors.white,
        title: Container(
          padding: const EdgeInsets.fromLTRB(55, 0, 0, 0),
          child: Row(
            children: const [
              Icon(
                Icons.restaurant_menu,
                color: Colors.black,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "Food Recipe",
                style: TextStyle(color: Colors.black),
              )
            ],
          ),
        ),
        actions: [
          IconButton(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            icon: const Icon(
              Icons.person,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return ProfilePage(email: widget.email.toString());
              }));
            },
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
          ),
        ],
      ),
      body: _isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : ListView.builder(
              itemCount: _recipe.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: (() {
                    Navigator.push(
                        (context),
                        MaterialPageRoute(
                            builder: (context) => DetailRecipe(
                                  name: _recipe[index].name,
                                  country: _recipe[index].country.toString(),
                                  totalTime:
                                      _recipe[index].totalTime.toString(),
                                  images: _recipe[index].images,
                                  description: _recipe[index].description,
                                  instructions: _recipe[index].instructions,
                                  sections: _recipe[index].sections,
                                )));
                  }),
                  child: RecipeCard(
                    title: _recipe[index].name,
                    country: _recipe[index].country.toString(),
                    cookTime: _recipe[index].totalTime.toString(),
                    thumbnailUrl: _recipe[index].images,
                  ),
                );
              },
            ),
      bottomNavigationBar: BottomNavigationBar(
        showSelectedLabels: false,
        showUnselectedLabels: false,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Colors.blue[700],
        selectedFontSize: 13,
        unselectedFontSize: 13,
        iconSize: 30,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.insert_chart),
            label: 'Activity',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.bookmark_outlined),
            label: 'Chats',
          ),
        ],
      ),
    );
  }
}
