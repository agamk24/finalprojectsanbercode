class Instructions {
  final String displayText;

  Instructions({required this.displayText});

  factory Instructions.fromJson(Map<String, dynamic> json) => Instructions(
      displayText: json['display_text'] != null ? json['display_text'] : '');
}

class Section {
  List<Component> components;

  Section({
    required this.components,
  });

  factory Section.fromJson(Map<String, dynamic> json) => Section(
        components: json['components'] != null
            ? List<Component>.from(
                json['components'].map((x) => Component.fromJson(x)))
            : [],
      );
}

class Component {
  final String rawText;

  Component({required this.rawText});

  factory Component.fromJson(Map<String, dynamic> json) =>
      Component(rawText: json['raw_text'] != null ? json['raw_text'] : '');
}

class Recipe {
  final String name;
  final String images;
  final String country;
  final String totalTime;
  final String description;
  final List<Instructions> instructions;
  final List<Section> sections;

  Recipe(
      {required this.name,
      required this.images,
      required this.country,
      required this.totalTime,
      required this.description,
      required this.instructions,
      required this.sections});

  factory Recipe.fromJson(dynamic json) {
    return Recipe(
        name: json["name"] as String,
        images: json["thumbnail_url"] as String,
        country: json["country"] as String,
        totalTime: json["total_time_minutes"] != null
            ? "${json['total_time_minutes']} Minutes"
            : "30 Minutes",
        description: json['description'],
        instructions: json['instructions'] != null
            ? List<Instructions>.from(
                json['instructions'].map((x) => Instructions.fromJson(x)))
            : [],
        sections: json['sections'] != null
            ? List<Section>.from(
                json['sections'].map((x) => Section.fromJson(x)))
            : []);
  }

  static List<Recipe> recipeFromSnapshot(List snapshot) {
    return snapshot.map((data) {
      return Recipe.fromJson(data);
    }).toList();
  }

  @override
  String toString() {
    return "Recipe {name : $name, image : $images, country : $country, totalTime : $totalTime}";
  }
}
